# Swifty Ghost

A very clean, minimalist theme for Ghost under the MIT license.

![](http://image.prntscr.com/image/3b9ab1dc35034ab08be45684639da634.jpg "Main screen")

Our blog is currently using it, [check it out](https://blog.cign.io) (It's in spanish)

## Focus

Our idea when working on this theme, was to provide the fastest experiencie
possible, but keeping a confortable reading on desktop, tablet or mobile.
Since this is our first open source contribution, there can be several rough
edges, style wise, license wise and so on.

To make sure the experience were as smooth as possible to the user, the set
our browser to throttle our connection speed to the slowest point possible,
to assure that even the user on the worst connection possible could have a
nice read and didn't have to wait several seconds for it to be available to
them.

## Installation

Clone this repo into ghost/content/themes, reload your Ghost instance and
activate the theme. Or download a zip file from this same page, or grab a
minified version copy from [here](https://drive.google.com/file/d/0B0MMzoaoMlgfSjNadHZUZmVpVkU/view?usp=sharing)
and upload it to your Ghost instance from the General tab on the admin page.

### Extending the theme

There are a couple of complements that can be enabled on this theme and extend
its functionality:

- To enable code highlighting, change the line **{{code-highlight}}** on
  **default.hbs**
- To enable comments, change the line **{{comments-SYSTEM}}** on **post.hbs**.
  It's important to note, you need to add the necessary key for the widget
  to work properly. There are three comment system available:
    - Google+
    - Facebook comments
    - Disqus
- To add Google Analytics to your blog, change the line **{{google-analytics}}**
  on **default.hbs**
- This one is a little weird and tricky. In order to enable the site header or
  logo on the post, you need to change the line that reads **{{#if false}}** to
  **{{#if true}}** on **post.hbs**.

### To-Do

- Rewrite CSS classes to use less space
- Add a build option to minify the CSS files and use even less space
- Add variants to the theme
- Any suggestion is welcome

### Issues and requests

Please feel free to file a new issue on this page if something is wrong or
needs an improvement.
